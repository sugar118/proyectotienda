{include file="template/header.tpl" title="header"}
<div id="content">
    <br>
    <h2>{$language->translate('new_product')}</h2>
    
    <form action="{$url}/{$lang}/product/insert" method="post">
    
        <label>{$language->translate('code')}</label><input type="text" name="codigo"><br>
        <label>{$language->translate('name')}</label><input type="text" name="nombre"><br>
        <label>{$language->translate('price')}</label><input type="text" name="precio">€<br>
        <label>{$language->translate('stock')}</label><input type="text" name="existencia"><br>
        
        
        <label></label><input type="submit" value="{$language->translate('send')}">
    </form>
    
</div>
{include file="template/footer.tpl" title="footer"}
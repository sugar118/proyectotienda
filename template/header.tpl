<!DOCTYPE html>

<html>
    <head>
        <title>TODO: supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="stylesheet" href="{$url}/public/css/default.css" />
        
        <script type="text/javascript" src="{$url}/js/jquery-1.11.2.min.js"></script>
        
        
        
    </head>
    <body>
        <div id="header">
            
            <div id="title">
                {$language->translate('app')} - {$language->translate('language')}
            </div>
            
            <div  style="float: left">
                <!--Estamos en el {$title}-->
                <a href="{$url}/{$lang}/index" >{$language->translate('index')}</a> 
                {if $idRole eq 3 }<a href="{$url}/{$lang}/login" >{$language->translate('login')}</a>{/if} 
                {if $idRole eq 3 }<a href="{$url}/{$lang}/register" >{$language->translate('register')}</a>{/if}
                <a href="{$url}/{$lang}/help" >{$language->translate('help')}</a>
                {if $idRole eq 1}<a href="{$url}/{$lang}/user" >{$language->translate('user')}</a>{/if}
                <a href="{$url}/{$lang}/product" >{$language->translate('product')}</a>
                {if $idRole neq 3}<a href="{$url}/{$lang}/pedido" >{$language->translate('order')}</a>{/if}
            </div>
                
            <div  style="float: right ; padding-left:4em">
                <a href="{$url}/es/index" >ES</a> 
                <a href="{$url}/en/index" >EN</a>
                
            </div>
        </div>
            

{include file="template/header.tpl" title="header"}

<script type="text/javascript" src="{$url}/js/funciones.js"></script>

<div id="content">
    <br>
    <h2>{$language->translate('product_list')}</h2>
    
    
    <!--<p><a href="{$url}/{$lang}/product/add">{$language->translate('new_product')}</a></p>-->
    {if $idRole == 1}
        <div style="text-align: center;">
            {$language->translate('searching')}
            <input type="text" name="buscar" style="width:50%;" id="valor">
        </div>
    {/if}
    <div id="log1"></div>
    <table border="1">
       
        <thead>
            <tr>
                <th>{$language->translate('code')}</th>
                <th>{$language->translate('name')}</th>
                <th>{$language->translate('price')}</th>
                <th>{$language->translate('stock')}</th>
                {if $idRole == 3}{else}<th>{$language->translate('operations')}</th>{/if}
            </tr>
        </thead>
        
        <tbody id="tbodyList">
           
            <tr>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                
                <br>
            </tr>
            
        </tbody>
        {if $idRole == 1}
        <tfoot id="nuevopr">
            <form action="{$url}/{$lang}/product/insert" method="post">
                <td> <input class="textInput" type="text" name="codigo"></td>
                <td> <input class="textInput" type="text" name="nombre"></td>
                <td> <input class="textInput" type="text" name="precio"></td>
                <td> <input class="textInput" type="text" name="existencia"></td>
                <td> <button class="textInput" type="submit">{$language->translate('accept')}</button></td>
            </form>
        </tfoot>
        {/if}
    </table>
                
    <span class="error">{$language->translate($error.code)}</span>
    <div id="paginas"></div>
    <div id="log"></div>
    
</div>
{include file="template/footer.tpl" title="footer"}
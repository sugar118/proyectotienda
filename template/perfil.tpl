{include file="template/header.tpl" title="header"}
<div id="content">
    <br>
    <h2>{$language->translate('profile')}</h2>
    
    <form action="{$url}/{$lang}/login/update" method="post">
        <input type="hidden" name="id" value="{$row.id}">
        <label>{$language->translate('name')}</label><input type="text" name="usuario" value="{$row.usuario}">  
        {$language->translate('role')}:  {if $idRole == 2}Usuario{else}Administrador{/if}<br>
        <label>{$language->translate('password')}</label><input type="password" name="password"><br>
        
        
        <br>
        
        <span class="error">{$language->translate($error.password)}</span> <br>
        <label></label><input type="submit" value="{$language->translate('send')}">
        
    </form>
    
</div>
{include file="template/footer.tpl" title="footer"}
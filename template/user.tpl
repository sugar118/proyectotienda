{include file="template/header.tpl" title="header"}
<div id="content">
    <br>
    <h2>{$language->translate('user_list')}</h2>
    <p><a href="{$url}/{$lang}/user/add">{$language->translate('new_user')}</a></p>
    <table border="1">
        <tr>
            <th>Id</th>
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('password')}</th>
            <th>{$language->translate('roles')}</th>
            <th>{$language->translate('operations')}</th>
        </tr>
            {foreach $rows as $row}
                <tr>
                    <td>{$row.id}</td>
                    <td>{$row.usuario}</td>
                    <td>{$row.password}</td>
                    <td>{$row.role}</td>
                    <td>
                        <a href="{$url}/{$lang}/user/edit/{$row.id}">{$language->translate('edit')}</a>
                        <a href="{$url}/{$lang}/user/delete/{$row.id}">{$language->translate('delete')}</a>
                    </td>
                </tr>
            {/foreach}
    </table>
    
    
</div>
{include file="template/footer.tpl" title="footer"}
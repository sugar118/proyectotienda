{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>{$language->translate('login')}</h2>
    
    <form action="{$url}/{$lang}/login/access" method="post">
        <label>{$language->translate('name')}</label><input type="text" name="name"><br>
        <label>{$language->translate('password')}</label><input type="password" name="password"><br>
        
        <br>
        <label></label><input type="submit" value="{$language->translate('send')}">
    </form>
    {$method}
 </div>
{include file="template/footer.tpl" title="footer"}
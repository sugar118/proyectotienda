{include file="template/header.tpl" title="header"}
<div id="content">
    <br>
    <h2>{$language->translate('register')}</h2>
    
    <form action="{$url}/{$lang}/register/newUser" method="post">
    
        <label>{$language->translate('name')}</label><input type="text" name="name"><br>
        <label>{$language->translate('password')}</label><input type="password" name="password"><br>
        
        <span class="error">{$language->translate($error.password)}</span>
        <br>
        <label></label><input type="submit" value="{$language->translate('send')}">
    </form>
    
</div>
{include file="template/footer.tpl" title="footer"}
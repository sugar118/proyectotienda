{include file="template/header.tpl" title="header"}

<div id="content">
    <br>
    <h2>{$language->translate('order')}</h2>
    
    
    <!--<p><a href="{$url}/{$lang}/product/add">{$language->translate('new_product')}</a></p>-->
    <form action="{$url}/{$lang}/pedido/finish" method="post">
        {foreach $rows as $row}
    <table border="1">
       
        <thead>
            <tr>
                <th>{$language->translate('id')}</th>
                <th>{$language->translate('fechaPedido')}</th>
                <th>{$language->translate('user')}</th>
            </tr>
        </thead>
        
        <tbody id="tbodyList">
                
                <tr>
                    <td>{$row.id}</td>
                    <td>{$row.fechaPedido}</td>
                    
                    {foreach $usuarios as $usuario}
                        
                        {if $row.idUsuario eq $usuario.id}
                            <td>{$usuario.nombre}</td>
                        {/if}
                        
                    {/foreach}
                </tr>
                
                <table border="1">

                <thead>
                    <tr>
                        
                        <th>{$language->translate('line')}</th>
                        <th>{$language->translate('product')}</th>
                        <th>{$language->translate('amount')}</th>
                        <th>{$language->translate('price')}</th>
                    </tr>
                </thead>
                <tbody>
                {foreach $rows1 as $row1}
                    {if $row1.idPedido == $row.id}
                    <tr>
                        
                        <td>{$row1.linea}</td>
                        
                        {foreach $productos as $producto}
                        
                            {if $row1.idProducto eq $producto.id}
                                <td>{$producto.nombre}</td>
                                {continue}
                            {/if}

                        {/foreach}
                        
                        
                        
                        <td>{$row1.cantidad}</td>
                        <td>{$row1.precio}</td>
                    </tr>
                    {/if}
                {/foreach}
                
                </tbody>
                </table><br><br>
            {/foreach}
        </tbody>
    </table>
        
    <span class="error">{$language->translate($error.code)}</span>
    <div id="paginas"></div>
    
</div>
{include file="template/footer.tpl" title="footer"}
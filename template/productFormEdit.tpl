{include file="template/header.tpl" title="header"}
<div id="content">
    <br>
    <h2>{$language->translate('edit_product')}</h2>
    
    <form action="{$url}/{$lang}/product/update" method="post">
        <input type="hidden" name="id" value="{$row.id}">
        
        <label>{$language->translate('code')}</label><input type="text" name="codigo" value="{$row.codigo}"><br>
        <label>{$language->translate('name')}</label><input type="text" name="nombre" value="{$row.nombre}"><br>
        <label>{$language->translate('price')}</label><input type="text" name="precio" value="{$row.precio}">€<br>
        <label>{$language->translate('stock')}</label><input type="text" name="existencia" value="{$row.existencia}"><br>
        
        
        <!--<span class="error">{$language->translate($error.password)}</span> <br>-->
        <label></label><input type="submit" value="{$language->translate('send')}">
        
    </form>
    
</div>
{include file="template/footer.tpl" title="footer"}
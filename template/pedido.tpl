{include file="template/header.tpl" title="header"}

<div id="content">
    <br>
    <h2>{$language->translate('order')}</h2>
    
    
    <!--<p><a href="{$url}/{$lang}/product/add">{$language->translate('new_product')}</a></p>-->
    <form action="{$url}/{$lang}/pedido/finish" method="post">
        <span style="float:right;">{$language->translate('preciofinal')}<span id="preciototal" >0</span><span>€</span></span>
    <table border="1">
       
        <thead>
            <tr>
                <th>{$language->translate('code')}</th>
                <th>{$language->translate('name')}</th>
                <th>{$language->translate('stock')}</th>
                <th>{$language->translate('amount')}</th>
                <th>{$language->translate('price')}</th>
                <th>{$language->translate('operations')}</th>
            </tr>
        </thead>
        
        <tbody id="tbodyList">
           {foreach $rows as $row}<form action="{$url}/{$lang}/pedido/finish" method="post">
               
               <input type="hidden" name="id{$row.id}" value="{$row.id}">
               <input type="hidden" name="precio{$row.id}" value="{$row.precio}">
               <input type="hidden" name="existencia{$row.id}" value="{$row.existencia}">
                <tr>
                    <td>{$row.codigo}</td>
                    <td>{$row.nombre}</td>
                    <td>{$row.existencia}</td>
                    <td><input class="textInput1" type="text" name="cantidad{$row.id}" id="cantidad{$row.id}" value="1"> <button type="button" class="button{$row.id}">+</button><button type="button" class="button{$row.id}">-</button></td>
                    <td><span id="precio{$row.id}">{$row.precio}</span><span>€</span></td>
                    <td>
                        <a href="{$url}/{$lang}/pedido/delete/{$row.id}">{$language->translate('delete')}</a>
                    </td>
                <br>
                </tr>
                <script>
                    
                    var precio{$row.id} = parseInt($("#precio{$row.id}").text());
                    var preciototal = $("#preciototal");
                    preciototal.text(parseInt(preciototal.text()) + precio{$row.id});
                    
                    $(".button{$row.id}").on("click", function() {
                        var $button = $(this);
                        var oldValue = $button.parent().find("input").val();
                        var spanprecio = $("#precio{$row.id}");
                        
                        
                        if ($button.text() == "+") {
                            var newVal = parseFloat(oldValue) + 1;
                            
                            if (newVal > {$row.existencia}) {
                               newVal = {$row.existencia};
                            } else {
                                spanprecio.text(parseInt(spanprecio.text()) + precio{$row.id});
                                preciototal.text(parseInt(preciototal.text()) + precio{$row.id});
                            }
                        } else {
                          // Don't allow decrementing below zero
                          if (oldValue > 1) {
                            var newVal = parseFloat(oldValue) - 1;
                            spanprecio.text(spanprecio.text() - precio{$row.id});
                            preciototal.text(parseInt(preciototal.text()) - precio{$row.id});
                          } else {
                            newVal = 1;
                          }
                          
                          //alert(spanprecio.text() * newVal);
                        }
                        
                        $button.parent().find("input").val(newVal);
                        
                      });
                    
                    
                    
                </script>
            {/foreach}
        </tbody>
    </table>
        
    <button class="textInput" type="submit">{$language->translate('accept')}</button>
    
    </form>
        
    <span class="error">{$language->translate($error.code)}</span>
    <div id="paginas"></div>
    
</div>
{include file="template/footer.tpl" title="footer"}
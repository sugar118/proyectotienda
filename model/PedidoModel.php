<?php

require_once 'lib/Model.php';

class PedidoModel extends Model {

    //Número de páginas total
    const PAGE_SIZE = 3;
    
    function __construct() {
        //echo 'En el UserModel<br>';
        parent::__construct();
    }

    public function delete($id) {
        $this->_sql = "DELETE FROM pedido WHERE id=$id";
        $this->executeQuery();
    }

    public function get($id) {
        $this->_sql = "SELECT producto.* "
                    . "FROM producto WHERE producto.id = $id";
        $this->executeSelect();
        return $this->_rows[0];
    }
    
    public function getAll() {
        $this->_sql = "SELECT pedido.* "
                    . "FROM pedido";
        $this->executeSelect();
        return $this->_rows;
    }
    
    public function getAllDetalles() {
        $this->_sql = "SELECT detallepedido.* "
                    . "FROM detallepedido";
        $this->executeSelect();
        return $this->_rows;
    }
    
    public function getUsuario($id) {
        $this->_sql = "SELECT usuario "
                    . "FROM usuarios WHERE id = $id";
        $this->executeSelect();
        return $this->_rows[0]["usuario"];
    }
    
    public function getProductos() {
        $this->_sql = "SELECT nombre,id "
                    . "FROM producto";
        $this->executeSelect();
        return $this->_rows;
        
    }
    
    public function insert($fila) {
        var_dump($fila);
        $this->_sql = "INSERT INTO pedido(codigo, nombre, precio, existencia) VALUES ('" . $fila[codigo] . "','" . $fila[nombre] . "','" . $fila[precio] . "','" . $fila[existencia] . "')";
        $this->executeQuery();
        
    }
    
    public function update($id) {
        $this->_sql = "UPDATE producto SET existencia='$nuevaexistencia' WHERE id = '$id'";
        $this->executeQuery();
    }
    
    public function updateExistencia($nuevaexistencia, $id) {
        $this->_sql = "UPDATE producto SET existencia='$nuevaexistencia' WHERE id = '$id'";
        $this->executeQuery();
    }
    
     public function finish($sql) {
        $this->_sql = $sql;
        $this->executeQuery();
    }
    
    public function getLinea() {
        $this->_sql = "SELECT MAX(id) FROM pedido";
        $this->executeSelect();
        return $this->_rows[0];
    }
    
    public function getidUsuario($nombre) {
        $this->_sql = "SELECT usuarios.id "
                    . "FROM usuarios WHERE usuarios.usuario = '" . $nombre . "'";
        $this->executeSelect();
        return $this->_rows[0];
    }
    
}


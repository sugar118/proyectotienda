<?php

require_once 'lib/Model.php';

class ProductModel extends Model {

    //Número de páginas total
    const PAGE_SIZE = 3;
    
    function __construct() {
        //echo 'En el UserModel<br>';
        parent::__construct();
    }

    public function delete($id) {
        $this->_sql = "DELETE FROM producto WHERE id = $id";
        $this->executeQuery();
    }

    public function get($id) {
        $this->_sql = "SELECT producto.* "
                    . "FROM producto WHERE producto.id = $id";
        $this->executeSelect();
        return $this->_rows[0];
    }

    public function getAll() {
        $this->_sql = "SELECT producto.* "
                    . "FROM producto";
        $this->executeSelect();
        return $this->_rows;
    }

    public function insert($fila) {
        var_dump($fila);
        $this->_sql = "INSERT INTO producto(codigo, nombre, precio, existencia) VALUES ('" . $fila[codigo] . "','" . $fila[nombre] . "','" . $fila[precio] . "','" . $fila[existencia] . "')";
        $this->executeQuery();
    }

    public function update($fila) {
        var_dump($fila);
        $this->_sql = "UPDATE producto SET codigo='$fila[codigo]', nombre='$fila[nombre]', precio='$fila[precio]', existencia='$fila[existencia]' WHERE id = '$fila[id]'";
        $this->executeQuery();
    }
    
    public function getBusqueda($buscar) {
        $this->_sql = "SELECT * "
                    . "FROM producto WHERE producto.nombre LIKE '%$buscar%'";
        $this->executeSelect();
        $page[0]  =   $this->_rows;
        $page[1]  =   $_SESSION["idRole"];
        $page[2]  =   $_SESSION["lang"];
        
        return $page;
    }
    
    
    public function getPageNumber($pageNumber,$lang) {
        if($_SESSION["idRole"] == 2 || $_SESSION["idRole"] == 3) {
            
            $this->_sql= "SELECT CEILING(COUNT(id)/" . $this::PAGE_SIZE . ") AS pages"
                . " FROM producto WHERE producto.existencia <> 0";
            
            $this->executeSelect();
        
            //Número de la página actual
            $page[0] = $pageNumber-1;

            //Páginas
            $page[1] = $this->_rows[0]['pages'];
            
            $this->_sql= "SELECT producto.* "
                   . "FROM producto WHERE producto.existencia <> 0 "
                   . "LIMIT " . $this::PAGE_SIZE . " OFFSET " . $page[0]*$this::PAGE_SIZE;
             //otro select que lea la pagina
        
            $this->executeSelect();

            $page[2]=  $this->_rows;

            $page[3] = $_SESSION["idRole"];
            $page[4] = $lang;

            return $page;
        } else {
            $this->_sql= "SELECT CEILING(COUNT(id)/" . $this::PAGE_SIZE . ") AS pages"
                . " FROM producto";
            
            $this->executeSelect();
        
            //Número de la página actual
            $page[0] = $pageNumber-1;

            //Páginas
            $page[1] = $this->_rows[0]['pages'];
            
            $this->_sql= "SELECT producto.* "
                   . "FROM producto "
                   . "LIMIT " . $this::PAGE_SIZE . " OFFSET " . $page[0]*$this::PAGE_SIZE;
             //otro select que lea la pagina
        
            $this->executeSelect();

            $page[2]=  $this->_rows;

            $page[3] = $_SESSION["idRole"];
            $page[4] = $lang;

            return $page;
        }
        
        
        
       
        
    }
    
}


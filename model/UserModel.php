<?php

require_once 'lib/Model.php';

class UserModel extends Model {

    function __construct() {
        //echo 'En el UserModel<br>';
        parent::__construct();
    }

    public function delete($id) {
        $this->_sql = "DELETE FROM usuarios WHERE id=$id";
        $this->executeQuery();
    }

    public function get($id) {
        $this->_sql = "SELECT usuarios.*, roles.role "
                    . "FROM usuarios INNER JOIN roles "
                    . "ON usuarios.idRole = roles.id "
                    . "WHERE usuarios.id = $id";
        $this->executeSelect();
        return $this->_rows[0];
    }

    public function getAll() {
        $this->_sql = "SELECT usuarios.*,roles.role "
                    . "FROM usuarios INNER JOIN roles "
                    . "ON usuarios.idRole = roles.id";
        $this->executeSelect();
        return $this->_rows;
    }

    public function insert($fila) {
        $this->_sql = "INSERT INTO usuarios(usuario, password, idRole) VALUES ('" . $fila[name] . "','" . $fila[password] . "','" . $fila[idRole] . "')";
        $this->executeQuery();
        
    }

    public function update($fila) {
        $this->_sql = "UPDATE usuarios SET usuario='$fila[name]', password='$fila[password]', idRole='$fila[idRole]' WHERE id = $fila[id]";
        $this->executeQuery();
    }

    public function usuario($usuario) {
        $this->_sql = "SELECT COUNT(usuario) AS count FROM usuarios WHERE usuario = '" . $usuario ."'";
        $this->executeSelect();
        return $this->_rows;
    }
    
}


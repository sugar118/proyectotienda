<?php
require_once 'lib/Model.php';

class LevelModel extends Model{

    function __construct()
    {
        parent::__construct();        
    }
    
    public function getAll()
    {
        $this->_sql = "SELECT *  FROM nivel ORDER BY id";
        $this->executeSelect();
        return $this->_rows;
    }

    protected function delete($numero)
    {
        
    }

    protected function get($numero)
    {
        
    }

    protected function insert($fila)
    {
        
    }

    protected function update($fila)
    {
        
    }

}


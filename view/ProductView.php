<?php

require_once 'lib/View.php';

class ProductView extends View {

    function __construct() {
        parent::__construct();
    }

    public function render($rows, $plantilla="product.tpl") {
        
        $this->smarty->display($plantilla);
    }
    
    public function add($error="") {
        $template='product.tpl';
        $this->smarty->assign('error', $error);
        $this->smarty->display($template);
    }
    
    public function edit($row, $error="") {
        $template='productFormEdit.tpl';
        $this->smarty->assign('row', $row);
        $this->smarty->assign('error', $error);
        $this->smarty->display($template);
    }
    
}


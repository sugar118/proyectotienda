<?php

require_once 'lib/View.php';

class PedidoView extends View {

    function __construct() {
        parent::__construct();
    }

    public function render($rows, $plantilla="pedido.tpl") {
        $this->smarty->assign('rows', $rows);
        $this->smarty->display($plantilla);
    }
    
    public function render1($rows,$rows1,$usuarios,$productos, $plantilla="pedido_1.tpl") {
        $this->smarty->assign('rows', $rows);
        $this->smarty->assign('rows1', $rows1);
        $this->smarty->assign('usuarios', $usuarios);
        $this->smarty->assign('productos', $productos);
        $this->smarty->display($plantilla);
    }
    
    public function method($plantilla="index.tpl") {
         $this->smarty->assign("method",  $this->getMethod());
         $this->smarty->display($plantilla);
    }
    
}


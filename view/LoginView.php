<?php

require_once 'lib/View.php';

class LoginView extends View{

    function __construct() {
        parent::__construct();
    }

    public function render($plantilla="login.tpl") {
        $this->smarty->assign("method",  $this->getMethod());
        $this->smarty->display($plantilla);
        
    }
    
    public function perfil($row, $plantilla="perfil.tpl") {
        $this->smarty->assign('row', $row);
        $this->smarty->assign("method",  $this->getMethod());
        $this->smarty->display($plantilla);
        
    }
}


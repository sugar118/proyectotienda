<?php

require_once 'lib/View.php';

class RegisterView extends View {

    function __construct() {
        parent::__construct();
    }
    
    public function render($error="") {
        $template='register.tpl';
        $this->smarty->assign('error', $error);
        $this->smarty->display($template);
    }
    
    public function render1($plantilla="index.tpl") {
        $this->smarty->assign("method",  $this->getMethod());
        $this->smarty->display($plantilla);
    }
}


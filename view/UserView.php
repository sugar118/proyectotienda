<?php

require_once 'lib/View.php';

class UserView extends View {

    function __construct() {
        parent::__construct();
    }

    public function render($rows, $plantilla="user.tpl") {
        //$this->smarty->assign("method",  $this->getMethod());
        $this->smarty->assign("rows",  $rows);
        $this->smarty->display($plantilla);
    }
    
    public function add($roles,$error="") {
        $template='userFormAdd.tpl';
        
        $this->smarty->assign('roles', $roles);
        $this->smarty->assign('error', $error);
        $this->smarty->display($template);
    }
    
    public function edit($row,$roles,$error="") {
        $template='userFormEdit.tpl';
        $this->smarty->assign('row', $row);
        $this->smarty->assign('error', $error);
        $this->smarty->assign('roles', $roles);
        $this->smarty->display($template);
    }
    
}


numero = 1;

$(document).ready(function(){
    
    
    var loadPages = function (numero) {
        $.post("http://localhost/proyectotienda/en/product/getAjaxPage/" + numero, function(data){
            
            page    =   data[0];
            pages   =   data[1];
            rows    =   data[2];
            idRole  =   data[3];
            lang    =   data[4];
            
            //alert(data);
            
            $("#tbodyList").empty();
            $("#paginas").empty();
            
            EditHref ="http://localhost/proyectotienda/en/product/edit/";
            BuyHref ="http://localhost/proyectotienda/en/pedido/add/";
            
            if(lang == "es"){
                boton1="Editar";
                boton2="Borrar";
                boton3="Comprar";
            }else if(lang == "en"){
                boton1="Edit";
                boton2="Remove";
                boton3="Buy";
            }
            
            for(var i=0; i<rows.length; i++){
               
                newRows =  '<tr class="cell" id="row' + rows[i].id + '">';
                newRows += '<td  id="rowCode' + rows[i].id + '">' + rows[i].codigo + '</td>';
                newRows += '<td  id="rowName' + rows[i].id + '">' + rows[i].nombre + '</td>';
                newRows += '<td  id="rowPrice' + rows[i].id + '">' + rows[i].precio + '</td>';
                newRows += '<td  id="rowStock' + rows[i].id + '">' + rows[i].existencia + '</td>';

                if(idRole == 1){
                    newRows += '<td>' + '<button id="delete' + rows[i].id + '">' + boton2 + '</button>';
                    newRows += '<a id="edit' + rows[i].id + '" href="'+EditHref+rows[i].id+'"><button>' + boton1 +'</button></a>' + '</td>';
                    
                    $("#delete" + rows[i].id).unbind('click');
                    $(document).on('click', '#delete'+rows[i].id,function(){
                        var idDelete = $(this).attr("id");
                        num = idDelete.substring(6);

                        deleteProduct(num);
                    });

                } else if(idRole == 2) {
                    newRows += '<td>' + '<a id="buy' + rows[i].id +'" href="'+BuyHref+rows[i].id+'"><button id="comprar" ">'+ boton3 +'</button></a>' + '</td>';
                }

                newRows += '</tr>';

                $( "#tbodyList" ).append(newRows);

                }
            
            loadButtons();
            
            
            
        }, 'json');
    };
    
    var loadButtons = function () {
        
        for(var z=0; z<pages; z++){
            pagina = z+1;
            newRows1 = '<button id="pagina' + pagina + '" onclick="">'+pagina+'</button>';
            $( "#paginas" ).append(newRows1);
            
            $("#pagina" + pagina).on('click',function(){
                var pagina = $(this).attr("id");
                numero = pagina.substring(6);
                $("#log1").empty();
                loadPages(numero);
                
            });
        }
        
    };
    
    var deleteProduct = function (num) {
      
        $.ajax({
            type: 'POST',
            url: "http://localhost/proyectotienda/en/product/delete/" + num,
            
            success:function(data) {
                if(data[0] != undefined){
                    $("#log1").empty();
                    $( "#log1" ).append("El producto que quieres borrar fue comprado por un cliente");
                } else {
                    $("#log1").empty();
                    $( "#log1" ).append("El producto ha sido borrado satisfactoriamente");
                    loadPages(numero);
                }
            }        
          });
    };
    
    var loadBusqueda = function (valor) {
        if (valor == "") {
            loadPages(numero);
        }
        $.post("http://localhost/proyectotienda/en/product/buscar/" + valor, function(data){
            //alert(data);
            rows    =   data[0];
            idRole  =   data[1];
            lang    =   data[2];
            
            //alert(lang);
            
            $( "#tbodyList" ).empty();
            $( "#paginas" ).empty();
            $("#log1").empty();
            EditHref ="http://localhost/proyectotienda/en/product/edit/";
            BuyHref ="http://localhost/proyectotienda/en/pedido/add/";
            
            if(lang == "es"){
                boton1="Editar";
                boton2="Borrar";
            }else if(lang == "en"){
                boton1="Edit";
                boton2="Remove";
            }
            
            for(var i=0; i<rows.length; i++){
                
                newRows =  '<tr class="cell" id="row' + rows[i].id + '">';
                newRows += '<td  id="rowCode' + rows[i].id + '">' + rows[i].codigo + '</td>';
                newRows += '<td  id="rowName' + rows[i].id + '">' + rows[i].nombre + '</td>';
                newRows += '<td  id="rowPrice' + rows[i].id + '">' + rows[i].precio + '</td>';
                newRows += '<td  id="rowStock' + rows[i].id + '">' + rows[i].existencia + '</td>';
               
                if(idRole == 1){
                    newRows += '<td>' + '<button id="delete' + rows[i].id + '">' + boton1 + '</button>';
                    newRows += '<a id="edit' + rows[i].id + '" href="'+EditHref+rows[i].id+'"><button>'+ boton2 +'</button></a>' + '</td>';
                    
                    $(document).on('click', '#delete'+rows[i].id,function(){
                        var idDelete = $(this).attr("id");
                        num = idDelete.substring(6);
                        deleteProduct(num);
                    });
                    
                } else if(idRole == 2) {
                    newRows += '<td>' + '<a id="buy' + rows[i].id +'" href="'+BuyHref+rows[i].id+'"><button id="comprar" ">'+ boton3 +'</button></a>' + '</td>';
                }
                
                newRows += '</tr>';
                
                $( "#tbodyList" ).append(newRows);
                
            }
            
            
        }, 'json');
    };
    
    $(document).keyup('#buscar',function(){
        var valor = $("#valor").val();
        loadBusqueda(valor);
        
    });
    
    loadPages(numero);
    
    
    
    
    
    
});
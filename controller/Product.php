<?php
require_once 'lib/Controller.php';

class Product extends Controller {

    public function __construct() {
        parent::__construct('Product');
//        echo "Dentro de Index<br>";
    }   
   
    public function index() {
        $rows = $this->model->getAll();
        $this->view->render($rows);
    }
    
    public function add($error) {
        
        $this->view->add($error);
    }
    
    public function insert() {
        $row = $_POST;
        
        $error = $this->_validate($row);
        var_dump($error);
        if (count($error)) {
            $this->add($error);
            
        } else {
            
            $this->model->insert($row);
            header("Location: " . Config::URL . "/" . $_SESSION["lang"] . "/product");
            
        }
        
    }
    
    public function delete($id) {
        $this->model->delete($id);
        
    }
    
    public function edit($id, $error="") {
        $row = $this->model->get($id);
        $this->view->edit($row, $error);
    } 
    
    public function update() {
        $row = $_POST;
        $this->model->update($row);    
        header('Location: ' . Config::URL . "/" . $_SESSION['lang'] . '/product/index');
        
    }
    
    
    private function _validate($row) {
        $error = array();
        
        foreach ($row as $key => $valor) {
            
            if($row[$key] ==  null || $zero){
                $error['code'] = 'error';
            }
        }
        if ($error['code'] == null){
            if (!preg_match("/^.{3,6}$/", $row['codigo'])){
                $error['code'] = 'error_code';
            }
        }
        
        
        return $error;
    }
    
    public function getAjaxPage($pageNumber){
        
        $paginas =  $this->model->getPageNumber($pageNumber, $_SESSION["lang"]);
        
        echo json_encode($paginas);
        
        exit();
        
    }
    
     public function buscar($buscar) {
        
        $busqueda =  $this->model->getBusqueda($buscar);
        
        echo json_encode($busqueda);
        
        exit();
     }
    
}
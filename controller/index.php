<?php

require_once "lib/Controller.php";
require_once 'view/IndexView.php';

class Index extends Controller{
    
    public $view;
    
    function __construct() {
        parent::__construct("Index");
        
    }
    
    public function method() {
        $this->view->setMethod("METHOD");
        $this->view->render();
    }
    
    public function hello($nombre="", $apellido="") {
        echo "Hello $nombre $apellido <br>";
    }
    
    public function index() {
        $this->view->render();
    }
}

<?php

require_once "lib/Controller.php";

class Error extends Controller{

    function __construct() {
        parent::__construct("Error");
        
    }
    
    public function methodFail() {
    }

    public function showMessage($message) {
        echo $message;
    }
    
    public function index() {
        $this->view->render();
    }
}
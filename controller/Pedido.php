<?php
require_once 'lib/Controller.php';
require_once 'view/PedidoView.php';

class Pedido extends Controller {

    public function __construct() {
        parent::__construct('Pedido');
//        echo "Dentro de Index<br>";
    }   
   
    public function finish(){
        
        $idUsuario = $this->model->getidUsuario($_SESSION["usuario"]);
        $sql = "INSERT INTO pedido(idUsuario) VALUES ('" . $idUsuario["id"] . "')";
        $this->model->finish($sql);
        
        foreach ($_POST as $clave => $valor) {
            if($clave == "id" . $valor){
                $id = $valor;
                $idPedido = $idPedido +1;
                
                $linea = $this->model->getLinea();
                if(is_null($linea["MAX(id)"])){
                    $linea = 0;
                } else {
                    $linea = $this->model->getLinea();
                }
                $sql1 = "INSERT INTO detallepedido(idPedido, linea, idProducto, precio, cantidad) VALUES ('" . $linea["MAX(id)"] . "','" . $idPedido . "','" . $id;
                
            } else if($clave == "precio" . $id) {
                $precio = $valor;
                $sql1 = $sql1 . "','" . $precio;
            } else if($clave == "existencia" . $id) {
                $existencia = $valor;
            } else if($clave == "cantidad" . $id) {
                $cantidad = $valor;
                $sql1 = $sql1 . "','" . $cantidad . "')";
                $nuevaexistencia = $existencia - $cantidad;
                $this->model->updateExistencia($nuevaexistencia,$id);
                $this->model->finish($sql1);
            }
        }
        unset($_SESSION["pedido"]);
        $this->view->setMethod("Sus productos han sido comprados satisfactoriamente.");
        $this->view->method();
        
    }
    
    public function index() {
        if($_SESSION["idRole"] == 2){
            if($_SESSION["pedido"]){
                $productos=$_SESSION["pedido"];
                foreach ($productos as $clave => $valor) {
                   $rows[]= $this->model->get($valor);
                }
                $this->view->render($rows);
            } else {
                $this->view->setMethod("No hay nada en la cesta.");
                $this->view->method();
            }
        } else if($_SESSION["idRole"] == 1){
            $pedidos= $this->model->getAll();
            $detalles= $this->model->getAllDetalles();
            
            foreach ($pedidos as $clave => $valor) {
                $usuarios[$clave]["nombre"]= $this->model->getUsuario($valor["idUsuario"]);
                $usuarios[$clave]["id"]= $valor["idUsuario"];
            }
            $productos = $this->model->getProductos();
            $this->view->render1($pedidos,$detalles,$usuarios,$productos);
            
        }
        
        
    }
    
    public function add($id) {
        $idAnterior = 0;
        foreach ($_SESSION["pedido"] as $clave => $valor) {
            $idAnterior = $valor;
            if($id == $idAnterior) {
                $this->view->setMethod("Este producto ya está en su cesta.");
                $this->view->method();
                exit();
            }
        }
        $_SESSION["pedido"][]=$id;
        header("Location: " . Config::URL . "/" . $_SESSION["lang"] . "/pedido");
    }
    
    public function insert() {
        $row = $_POST;
        
        $error = $this->_validate($row);
        var_dump($error);
        if (count($error)) {
            $this->add($error);
            
        } else {
            
            $this->model->insert($row);
            header("Location: " . Config::URL . "/" . $_SESSION["lang"] . "/product");
            
        }
        
    }
    
    public function delete($id) {
        foreach ($_SESSION["pedido"] as $clave => $valor) {
            if ($valor == $id){
                unset($_SESSION["pedido"][$clave]);
                header("Location: " . Config::URL . "/" . $_SESSION["lang"] . "/pedido");
                exit();
            }
        }
    }
    
    public function edit($id, $error="") {
        $row = $this->model->get($id);
        $this->view->edit($row, $error);
    } 
    
    public function update() {
        $row = $_POST;
        $this->model->update($row);    
        header('Location: ' . Config::URL . "/" . $_SESSION['lang'] . '/product/index');
        
    }
}

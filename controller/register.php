<?php

require_once "lib/Controller.php";
require_once 'view/RegisterView.php';

class Register extends Controller{
    
    public $view;
    
    function __construct() {
        parent::__construct("Register");
        
    }
    
    public function method($method) {
        $this->view->setMethod($method);
        $this->view->render1();
    }
    
    public function index($error="") {
        if($_SESSION["idRole"] == 3) {
            if($error == null){
                $error['password'] ="";
            } 
            $this->view->render($error);
        }
    }
    
    public function newUser() {
        if($_SESSION["idRole"] == 3) {
            $row = $_POST;
            
            $error = $this->_validate($row);
            
            if (count($error)){
                
                $this->index($error);
                
            } else {
            
                $row['password'] = md5($row['password']);
                $this->model->insert($row);

                $this->method("El usuario $row[name] ha sido registrado satisfactoriamente.");
            }
            
        } else {
            header("Location: " . Config::URL . "/" . $_SESSION["lang"] . "/index");
        }
        
    }
    
    private function _validate($row) {
        $error = array();
        $usuario1 = $this->model->usuario($row["name"]);
        $usuario = $usuario1[0]["count"];
        if (!preg_match("/^.{3,10}$/", $row['name'])){
            $error['password'] = 'error_user';
        } else if(!preg_match("/^.{6,20}$/", $row['password'])){
            $error['password'] = 'error_password';
        } else if($usuario != 0){
            $error['password'] = 'error_user1';
        }
        
        return $error;
    }
    
}

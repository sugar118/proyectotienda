<?php
require_once 'lib/Controller.php';
require_once 'model/RoleModel.php';

class User extends Controller {

    public function __construct() {
        parent::__construct('User');
//        echo "Dentro de Index<br>";
    }   
   
    public function index() {
        $rows = $this->model->getAll();
        $this->view->render($rows);
    }
    
    public function add($error="") {
        $roleModel = new RoleModel();
        $roles = $roleModel->getAll(false);
        if($error == null){
            $error['password'] ="";
        } 
         
        $this->view->add($roles,$error);
    }
    
    public function insert() {
        $row = $_POST;
        
        $error = $this->_validate($row,1);
        
        if (count($error)) {
            $this->add($error);
            
        } else {
        
            $row['password'] = md5($row['password']);
            $this->model->insert($row);
            header("Location: " . Config::URL . "/" . $_SESSION["lang"] . "/user");
        
        }
        
    }
    
    public function delete($id) {
        $this->model->delete($id);
        header("Location: " . Config::URL . "/" . $_SESSION["lang"] . "/user");
    }
    
    public function edit($id, $error="") {
        
        if($error == null){
            $error['password'] ="";
        } 
        
        $roleModel = new RoleModel();
        $roles = $roleModel->getAll(false);
        $row = $this->model->get($id);
        $this->view->edit($row, $roles,$error);
    } 
    
    public function update() {
        
        $row = $_POST; 
        $error = $this->_validate($row,2);
        
        if (count($error)){
            $this->edit($row['id'], $error);
        } else {
            $row['password'] = md5($row['password']);
            $this->model->update($row);    
            header('Location: ' . Config::URL . "/" . $_SESSION['lang'] . '/user/index');
        }
    }
    
    private function _validate($row,$id1) {
        $error = array();
        $zero = "0";
        $usuario1 = $this->model->usuario($row["name"]);
        $usuario = $usuario1[0]["count"];
        
        foreach ($row as $key => $valor) {
            if($row[$key] ==  null || $zero){
                $error['password'] = 'error';
            }
        }
        
        if($row["idRole"] == $zero){
            $error['password'] = 'error';
        }
        
        if ($error['password'] == null){
            if($id1 == 1) {
                if (!preg_match("/^.{3,10}$/", $row['name'])){
                    $error['password'] = 'error_user';
                } else if(!preg_match("/^.{6,20}$/", $row['password'])){
                    $error['password'] = 'error_password';
                } else if($usuario != 0){
                    $error['password'] = 'error_user1';
                }
            } else if($id1 == 2){
                $nombre = $this->model->get($row["id"]);
                $nombre1 = $nombre["usuario"];
                if (!preg_match("/^.{3,10}$/", $row['name'])){
                    $error['password'] = 'error_user';
                } else if(!preg_match("/^.{6,20}$/", $row['password'])){
                    $error['password'] = 'error_password';
                } else if($row['name'] == $nombre1){
                    
                } else if($usuario != 0){
                    $error['password'] = 'error_user1';
                }
            }
        }
        
        return $error;
    }
}
<?php

require_once "lib/Controller.php";
require_once 'view/LoginView.php';

class Login extends Controller{
    
    public $view;
    
    function __construct() {
        parent::__construct("Login");
    }
    
    public function access() {
        if($_SESSION["idRole"] == 3) {
            $row = $_POST;
            $row['password'] = md5($row['password']);

            $userLogin=$this->model->get($row);
            //var_dump($row);

            if($userLogin[idRole] != null) {
                $_SESSION["usuario"] = $row["name"];
                $_SESSION["idRole"] = $userLogin["idRole"];
                //var_dump($row);
                //var_dump($_SESSION);
                header("Location: " . Config::URL . "/" . $_SESSION["lang"] . "/index");
                //$this->method("El usuario $row[name] ha realizado satisfactoriamente el login.");

            } else {
                $this->method("No se ha realizado satisfactoriamente el login.");
            }
        } else {
            header("Location: " . Config::URL . "/" . $_SESSION["lang"] . "/index");
        }
    }
    
    public function perfil() {
        $perfil = $this->model->getPerfil($_SESSION["usuario"]);
        $this->view->perfil($perfil);
    }
    
    public function update() {
        $this->model->update($_POST);
        $_SESSION["usuario"] = $_POST["usuario"];
        $perfil = $this->model->getPerfil($_SESSION["usuario"]);
        $this->view->perfil($perfil);
    }
    
    public function disconnect() {
        
        $_SESSION['usuario']="Anonymous";
        $_SESSION['idRole']=3;
        unset($_SESSION["pedido"]);
        header("Location: " . Config::URL . "/" . $_SESSION["lang"] . "/index");
    }
    
    
    public function method($method) {
        $this->view->setMethod($method);
        $this->view->render();
    }
    
    public function index() {
        if($_SESSION["idRole"] != 3) {
            header("Location: " . Config::URL . "/" . $_SESSION["lang"] . "/index");
        } else {
            $this->view->render();
        }
        
    }
}
